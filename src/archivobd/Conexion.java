/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivobd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Administrador
 */
public class Conexion {

    private static Connection cnn;

    private Conexion() {
    }

    private static void conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/conexion", "root", "");
        } catch (SQLException e) {
            System.out.println("Error de MySQL: "
                    + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error inesperado: "
                    + e.getMessage());
        }
    }

    public static Connection getInstace() {
        if (cnn == null) {
            conectar();
        }
        return cnn;
    }

}
