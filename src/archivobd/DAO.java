/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivobd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Administrador
 */
public class DAO implements InsertarIn {

    private Connection cnn;
    private PreparedStatement pstmt;

    public DAO() {
        cnn = Conexion.getInstace();
    }

    public String insertar(cadenaDTO c) {
        int salida = 0;
        String mensaje = "";
        try {
            pstmt = cnn.prepareStatement("insert into user values (?)");
            pstmt.setString(1, c.getPalabras());
            salida = pstmt.executeUpdate();
            if (salida != 0) {
                mensaje = "se inserto";
            } else {
                mensaje = "No se inserto";
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally{
            System.out.println(mensaje);
        }
        return mensaje;
    }

    public String mostrar() {
        String mensaje = "";

        Statement st;
        ResultSet rs;
        String sql = "select * from user";
        try {
            st = cnn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                System.out.println("Nombre: " + rs.getString(1));
            }
//    cnn.close();
//    rs.close();
//    st.close();
        } catch (SQLException e) {
            System.out.println("error " + e);
        } finally {
       
        }
        return mensaje;
    }

}
