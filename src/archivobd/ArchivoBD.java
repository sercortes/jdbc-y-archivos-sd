
package archivobd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class ArchivoBD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, NumberFormatException {
        // TODO code application logic here
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            int opcion;
            String palabra;

            do {
                System.out.println("");
                System.out.println("elija una opcion");
                System.out.println("1.Insertar palabra en la  BD");
                System.out.println("2.leer la BD");
                System.out.println("3.Insertar Palabras en  Archivo");
                System.out.println("4.para leer el archivo");
                System.out.println("5.para salir");

                opcion = Integer.parseInt(in.readLine());
                Factory f = new Factory();

                switch (opcion) {

                    case 1:
                        if (Conexion.getInstace() != null) {
                            System.out.println("se conecto");
                            System.out.println("Escriba el nombre a insertar");
                            palabra = in.readLine();
                            cadenaDTO c1 = new cadenaDTO(palabra);
                            f.getInsertar("1", c1);
                        } else {
                            System.out.println("error en conexion, sin conectarse");
                        }
                        break;

                    case 2:
                        f.leer("2");
                        break;

                    case 3:
                          System.out.println("Ingrese las palabras a insertar");
                          palabra= in.readLine();
                          cadenaDTO c1 = new cadenaDTO(palabra);
                          f.getInsertar("3", c1);
                        break;
                    case 4:
                        f.leer("4");

                        break;

                    case 5:
                        System.out.println("saliendo");
                        break;
                    default:
                        System.out.println("Ingrese una opcion correcta");
                    break;
                }

            } while (opcion != 5);
            System.out.println("");
        } catch (NumberFormatException ex) {
            System.out.println("ingrese la opción correcta, son numeros del 1 a 5");
        }
    }
}
