/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivobd;

import java.io.Serializable;

/**
 *
 * @author Administrador
 */
public class cadenaDTO implements Serializable{
    private String palabras;

    public String getPalabras() {
        return palabras;
    }

    public void setPalabras(String palabras) {
        this.palabras = palabras;
    }

    @Override
    public String toString() {
        return "cadena{" + "palabras=" + palabras + '}';
    }

    public cadenaDTO(String palabras) {
        this.palabras = palabras;
    }
    
}
