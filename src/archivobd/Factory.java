/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivobd;

/**
 *
 * @author Administrador
 */
public class Factory {

    public static String getInsertar(String ti, cadenaDTO c1) {
        if (ti.equals("1")) {
            return new DAO().insertar(c1);
        } else if (ti.equals("3")) {
            return new InsertarAr().insertar(c1);
        }
        return null;
    }

    public static String leer(String ti) {
        if (ti.equals("2")) {
            return new DAO().mostrar();
        }else if(ti.equals("4")){
            return new InsertarAr().mostrar();
        }
        return null;
    }
}
